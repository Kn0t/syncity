/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#ifndef GAME_LAYERS_H
#define GAME_LAYERS_H

#include <engine/map.h>
#include <game/mapitems.h>

class CLayers
{
	int m_GroupsNum;
	int m_GroupsStart;
	int m_LayersNum;
	int m_LayersStart;
	CMapItemGroup *m_pGameGroup;
	CMapItemLayerTilemap *m_pGameLayer;
	class IMap *m_pMap;

	void InitGameLayer();
	void InitTilemapSkip();

	// darktown
	CMapItemLayerTilemap *m_pSubGameLayers[16];
	CMapItemLayerTilemap *m_pIDsLayer;

	inline void IntsToStr(const int *pInts, int Num, char *pStr) {
		while(Num)
		{
			pStr[0] = (((*pInts)>>24)&0xff)-128;
			pStr[1] = (((*pInts)>>16)&0xff)-128;
			pStr[2] = (((*pInts)>>8)&0xff)-128;
			pStr[3] = ((*pInts)&0xff)-128;
			pStr += 4;
			pInts++;
			Num--;
		}

		// null terminate
		pStr[-1] = 0;
	}

public:
	CLayers();
	void Init(class IKernel *pKernel, class IMap *pMap=0);
	int NumGroups() const { return m_GroupsNum; };
	int NumLayers() const { return m_LayersNum; };
	class IMap *Map() const { return m_pMap; };
	CMapItemGroup *GameGroup() const { return m_pGameGroup; };
	CMapItemLayerTilemap *GameLayer() const { return m_pGameLayer; };
	CMapItemGroup *GetGroup(int Index) const;
	CMapItemLayer *GetLayer(int Index) const;

    // darktown
	int MAX_GAME_LAYERS = 16;
	int m_SubGameLayerCount = 0;

	CMapItemLayerTilemap *IDLayer() const { return m_pIDsLayer; };
	CMapItemLayerTilemap *SubGameLayers(int i) const { return m_pSubGameLayers[i]; };
};

#endif
