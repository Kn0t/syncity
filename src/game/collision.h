/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#ifndef GAME_COLLISION_H
#define GAME_COLLISION_H

#include <base/vmath.h>
#include <game/mapitems.h>

class CCollision
{
	class CTile *m_pTiles;
	int m_Width;
	int m_Height;
	class CLayers *m_pLayers;

	bool IsTile(int x, int y, int Type=TILE_SOLID) const;
	int GetTile(int x, int y) const;

	//darktown
	class CTile *m_pAllTiles[17];
	class CTile *m_pIDTile;
	class CDoorTile* m_pDoorTiles[2];

	class CConfig *m_pConfig;

	int m_TotalTiles = 1;
	bool m_DoorOpen[2][1024];

public:
	enum
	{
		COLFLAG_SOLID=1,
		COLFLAG_DEATH=2,
		COLFLAG_NOHOOK=4,
	};

	CCollision();
	void Init(class CLayers *pLayers);
	bool CheckPoint(float x, float y, int Type=TILE_SOLID) const { return IsTile(round_to_int(x), round_to_int(y), Type); }
	bool CheckPoint(vec2 Pos, int Type=TILE_SOLID) const { return CheckPoint(Pos.x, Pos.y, Type); }
	int GetCollisionAt(float x, float y) const { return GetTile(round_to_int(x), round_to_int(y)); }
	int GetWidth() const { return m_Width; };
	int GetHeight() const { return m_Height; };
	int IntersectLine(vec2 Pos0, vec2 Pos1, vec2 *pOutCollision, vec2 *pOutBeforeCollision) const;
	void MovePoint(vec2 *pInoutPos, vec2 *pInoutVel, float Elasticity, int *pBounces) const;
	void MoveBox(vec2 *pInoutPos, vec2 *pInoutVel, vec2 Size, float Elasticity, bool *pDeath=0) const;
	bool TestBox(vec2 Pos, vec2 Size, int Type=COLFLAG_SOLID) const;

	// darktown
	int GetID(int x, int y) const;
	int GetID(float x, float y) const { return GetID(round_to_int(x), round_to_int(y)); }
	int GetID(vec2 pos) const { return GetID(pos.x, pos.y); }

	int SetDoorAt(vec2 From, vec2 To, int Number, int Type);
	bool GetDoorAt(int x, int y) const;
	bool SetDoorState(int DoorNumber, bool Open, int Type);
	bool IsDoorOpen(int DoorNumber, int Type) { return  m_DoorOpen[Type][DoorNumber]; }

	enum {
		DOOR_PUBLIC,
		DOOR_ID,

		NUM_DOORS
	};
};

#endif
