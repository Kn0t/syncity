/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#ifndef GAME_SERVER_GAMEMODES_DARK_H
#define GAME_SERVER_GAMEMODES_DARK_H
#include <game/server/gamecontroller.h>

class CGameControllerDark : public IGameController
{
private:
	// class CApi *m_pApi;

	// class CApi *Api() { return m_pApi; }

	void DoActivityCheck();

	class CSocket *m_pSocket;

public:
	CGameControllerDark(class CGameContext *pGameServer);

	virtual void Tick();
	virtual bool OnEntity(int Index, vec2 Pos);

	static void Com_Login(IConsole::IResult *pResult, void *pContext);
	static void Com_Register(IConsole::IResult *pResult, void *pContext);
	static void Com_Tele(IConsole::IResult *pResult, void *pContext);
	static void Com_Top10(IConsole::IResult *pResult, void *pContext);
	// info cmds
	static void Com_Me(IConsole::IResult *pResult, void *pContext);
	static void Com_Info(IConsole::IResult *pResult, void *pContext);

	virtual void RegisterChatCommands(CCommandManager *pManager);
};

#endif
