/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#include "darktown.h"

#include <engine/shared/config.h>
#include <game/server/gamecontext.h>
#include <game/server/player.h>
#include <game/server/entities/character.h>
#include <game/collision.h>
#include <game/server/darktown/api.h>
#include <game/server/darktown/socket.h>
#include <game/mapitems.h>

#include "game/server/entities/pickup.h"
#include "game/server/darktown/entities/door.h"
#include "game/server/darktown/entities/trigger.h"
#include "game/server/darktown/entities/teleport.h"

CGameControllerDark::CGameControllerDark(CGameContext *pGameServer)
: IGameController(pGameServer)
{
	m_pGameType = "SynCity";
	m_pApi = new CApi(pGameServer);
	m_pSocket = new CSocket(pGameServer);
	thread_init(m_pSocket->Run, m_pSocket);
}

bool CGameControllerDark::OnEntity(int Index, vec2 Pos) {

	if(IGameController::OnEntity(Index, Pos))
		return true;
	
	if(Index == ENTITY_DOOR_ID || Index == ENTITY_DOOR_NR)
		new CDoor(&GameServer()->m_World, Pos, Index == ENTITY_DOOR_ID ? CCollision::DOOR_ID : CCollision::DOOR_PUBLIC);
	else if(Index == ENTITY_TRIGGER_ID || Index == ENTITY_TRIGGER_NR)
		new CTrigger(&GameServer()->m_World, Pos, Index == ENTITY_TRIGGER_ID);
	else if(Index == ENTITY_TEL_FROM)
		new CTeleport(&GameServer()->m_World, Pos, true);
	else if(Index == ENTITY_TEL_TO)
	{
		new CTeleport(&GameServer()->m_World, Pos, false);
		GameServer()->m_TeleNum++;
	}
}


void CGameControllerDark::Tick() {
	DoActivityCheck();
}

//activity
void CGameControllerDark::DoActivityCheck()
{
	if(Config()->m_SvInactiveKickTime == 0)
		return;

	for(int i = 0; i < MAX_CLIENTS; ++i)
	{
		if(GameServer()->m_apPlayers[i] && !GameServer()->m_apPlayers[i]->IsDummy() && (GameServer()->m_apPlayers[i]->GetTeam() != TEAM_SPECTATORS || Config()->m_SvInactiveKick > 0) &&
			!Server()->IsAuthed(i) && (GameServer()->m_apPlayers[i]->m_InactivityTickCounter > Config()->m_SvInactiveKickTime*Server()->TickSpeed()*60))
		{
			switch(Config()->m_SvInactiveKick)
			{
			case 1:
				{
					// move player to spectator
					DoTeamChange(GameServer()->m_apPlayers[i], TEAM_SPECTATORS);
				}
				break;
			case 2:
				{
					// move player to spectator if the reserved slots aren't filled yet, kick him otherwise
					int Spectators = 0;
					for(int j = 0; j < MAX_CLIENTS; ++j)
						if(GameServer()->m_apPlayers[j] && GameServer()->m_apPlayers[j]->GetTeam() == TEAM_SPECTATORS)
							++Spectators;
					if(Spectators >= Config()->m_SvMaxClients - Config()->m_SvPlayerSlots)
						Server()->Kick(i, "Kicked for inactivity");
					else
						DoTeamChange(GameServer()->m_apPlayers[i], TEAM_SPECTATORS);
				}
				break;
			case 3:
				{
					// kick the player
					Server()->Kick(i, "Kicked for inactivity");
					break;
				}
			case 4:
				{
					// send to afk room			
					if (!GameServer()->m_apPlayers[i]->m_IsAfk)
						GameServer()->m_apPlayers[i]->SendAfk();

					break;
				}
			default: {
					dbg_msg("DEBUG", "Unkown case");
				}
			}
		} else if (GameServer()->m_apPlayers[i] && GameServer()->m_apPlayers[i]->m_LastActionTick == Server()->Tick()) {
			GameServer()->m_apPlayers[i]->m_IsAfk = false;
		}
	}
}

void CGameControllerDark::Com_Login(IConsole::IResult *pResult, void *pContext)
{
	CCommandManager::SCommandContext *pComContext = (CCommandManager::SCommandContext *)pContext;
	IGameController *pSelf = (IGameController *)pComContext->m_pContext;

	pSelf->Api()->Login(pResult->GetString(0), pResult->GetString(1), pSelf->GameServer()->m_apPlayers[pComContext->m_ClientID]);
}

void CGameControllerDark::Com_Register(IConsole::IResult *pResult, void *pContext)
{
	CCommandManager::SCommandContext *pComContext = (CCommandManager::SCommandContext *)pContext;
	IGameController *pSelf = (IGameController *)pComContext->m_pContext;

	pSelf->Api()->Register(pResult->GetString(0), pResult->GetString(1), pSelf->GameServer()->m_apPlayers[pComContext->m_ClientID]);
}

void CGameControllerDark::Com_Tele(IConsole::IResult *pResult, void *pContext)
{
	CCommandManager::SCommandContext *pComContext = (CCommandManager::SCommandContext *)pContext;
	IGameController *pSelf = (IGameController *)pComContext->m_pContext;

	CCharacter *pChr = pSelf->GameServer()->GetPlayerChar(pComContext->m_ClientID);

	if (!pChr)
		return;

	if (pChr->m_GameMode) {
		pSelf->GameServer()->SendChatTarget(pComContext->m_ClientID, "You can't tele while your in race");
		return;
	}

	vec2 TargetPos = pChr->GetPos() + vec2(pChr->m_Core.m_Input.m_TargetX, pChr->m_Core.m_Input.m_TargetY);

	if (pSelf->GameServer()->Collision()->CheckPoint(TargetPos))
		return;

	float Distance = distance(TargetPos, pChr->GetPos());
	
	if (Distance > 500) {
		pSelf->GameServer()->SendChatTarget(pComContext->m_ClientID, "You can't tele that far");
		return;
	}

	pChr->m_Core.m_Pos = TargetPos;
	CNetEvent_Spawn* pEvent = (CNetEvent_Spawn*)pSelf->GameServer()->m_Events.Create(NETEVENTTYPE_SPAWN, sizeof(CNetEvent_Spawn));
	if (pEvent)
	{
		pEvent->m_X = (int)TargetPos.x;
		pEvent->m_Y = (int)TargetPos.y;
	}
}

void CGameControllerDark::Com_Top10(IConsole::IResult *pResult, void *pContext)
{
	CCommandManager::SCommandContext *pComContext = (CCommandManager::SCommandContext *)pContext;
	IGameController *pSelf = (IGameController *)pComContext->m_pContext;

	pSelf->Api()->Top10(pComContext->m_ClientID, "race");
}

// info cmds
void CGameControllerDark::Com_Me(IConsole::IResult *pResult, void *pContext)
{
	CCommandManager::SCommandContext *pComContext = (CCommandManager::SCommandContext *)pContext;
	IGameController *pSelf = (IGameController *)pComContext->m_pContext;
	CPlayer *pPlayer = pSelf->GameServer()->m_apPlayers[pComContext->m_ClientID];
	char aBuf[256], aMoney[16], aExp[16];

	if (!pPlayer)
		return;

	str_format_int(pPlayer->m_AccData.Money, aMoney);
	str_format_int(pPlayer->m_AccData.Exp, aExp);

	pSelf->GameServer()->SendChatTarget(pComContext->m_ClientID, "~~~ Account Data ~~~");
	str_format(aBuf, sizeof(aBuf), "Level: %d", pPlayer->m_AccData.Level);
	pSelf->GameServer()->SendChatTarget(pComContext->m_ClientID, aBuf);
	str_format(aBuf, sizeof(aBuf), "Exp: %sep", aExp);
	pSelf->GameServer()->SendChatTarget(pComContext->m_ClientID, aBuf);
	str_format(aBuf, sizeof(aBuf), "Money: %s$", aMoney);
	pSelf->GameServer()->SendChatTarget(pComContext->m_ClientID, aBuf);
}

void CGameControllerDark::Com_Info(IConsole::IResult *pResult, void *pContext)
{
	CCommandManager::SCommandContext *pComContext = (CCommandManager::SCommandContext *)pContext;
	IGameController *pSelf = (IGameController *)pComContext->m_pContext;
	CPlayer *pPlayer = pSelf->GameServer()->m_apPlayers[pComContext->m_ClientID];
	char aBuf[256], aMoney[16], aExp[16];

	if (!pPlayer)
		return;

	pSelf->GameServer()->SendChatTarget(pComContext->m_ClientID, "~~~ Info ~~~");
	pSelf->GameServer()->SendChatTarget(pComContext->m_ClientID, "Syn | City is created by UrinStone.\nThis mod is currently in an alpha testing phase.\nPlease join our Discord for more information https://discord.gg/qcUwT92Jr4");
}

void CGameControllerDark::RegisterChatCommands(CCommandManager *pManager)
{
	pManager->AddCommand("login", "Login to your account", "ss", Com_Login, this);
	pManager->AddCommand("register", "Register to the city", "ss", Com_Register, this);
	pManager->AddCommand("tele", "Teleports you to your cursor", "", Com_Tele, this);
	pManager->AddCommand("top10", "Top 10 ranking", "?s", Com_Top10, this);
	// info cmds
	pManager->AddCommand("me", "Returns your account information", "", Com_Me, this);
	pManager->AddCommand("info", "Information about the server", "", Com_Info, this);

}
