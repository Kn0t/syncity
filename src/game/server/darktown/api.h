#ifndef GAME_SERVER_DARKTOWN_API
#define GAME_SERVER_DARKTOWN_API

class CApi {
private:
    class CGameContext *m_pGameServer;

    class CGameContext *GameServer() { return m_pGameServer; }

    bool m_Timeout;

public:
    CApi(CGameContext *pGameServer);

    bool Login(const char *username, const char *password, class CPlayer *pPlayer);
    bool Register(const char *username, const char *password, class CPlayer *pPlayer);
    void Top10(int ClientID, const char *List);

    void GetAccountData(class CPlayer *pPlayer);
    void UpdateAccount(class CPlayer *pPlayer);
};

#endif