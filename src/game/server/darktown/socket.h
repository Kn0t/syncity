#ifndef GAME_SERVER_DARKTOWN_SOCKET
#define GAME_SERVER_DARKTOWN_SOCKET

#include "engine/server/websocket.h"
#include <iostream>
#include <thread>
#include <atomic>
#include <mutex>
#include <signal.h>


class CSocket {
private:
    using WSClient = websocket::WSClient<CSocket>;
    using WSConn = WSClient::Connection;

    WSClient m_WSclient;
    std::thread m_WSThread;
    std::mutex m_Mtx;
    std::string m_AdmincmdHelp;
    std::atomic<bool> m_Running;

    class CGameContext *m_pGameServer;

    class CGameContext *GameServer() { return m_pGameServer; }

    void ParseMessage(const char *Payload, char *Out);
    void HandleMessage(const char *Msg);

public:
    CSocket(CGameContext *pGameServer);

    static void Run(void *pUser);
    void Stop() { m_Running = false; }
    void onWSClose(WSConn& Conn, uint16_t StatusCode, const char* Reason);
    void onWSMsg(WSConn& Conn, uint8_t OPcode, const uint8_t* Payload, uint32_t PLLen);
    void onWSSegment(WSConn& Conn, uint8_t OPcode, const uint8_t* Payload, uint32_t PLLen, uint32_t PLStartIdx, bool Fin);
};

#endif