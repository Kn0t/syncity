/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */

#include <game/server/gamecontext.h>
#include <game/server/gamecontroller.h>
#include "game/server/entities/character.h"
#include "protection.h"

CProtection::CProtection(CGameWorld *pGameWorld, vec2 Pos, int Owner)
: CEntity(pGameWorld, CGameWorld::ENTTYPE_PICKUP, Pos)
{
	m_CID = Owner;
    m_pOwner = GameServer()->GetPlayerChar(m_CID);

	GameWorld()->InsertEntity(this);
}

void CProtection::Reset()
{
	GameServer()->m_World.DestroyEntity(this);
}

void CProtection::Tick()
{
	if(!m_pOwner)
	{
		Reset();
		return;
	}

	if(!m_pOwner->m_Protected)
	{
		return;
	}

	m_Pos = m_pOwner->m_Pos;
	m_Pos.y -= 32;
}


void CProtection::Snap(int SnappingClient)
{
    if(!m_pOwner->m_Protected)
	{
		return;
	}

	if(NetworkClipped(SnappingClient, m_Pos))
		return;

	CNetObj_Pickup *pP = static_cast<CNetObj_Pickup *>(Server()->SnapNewItem(NETOBJTYPE_PICKUP, GetID(), sizeof(CNetObj_Pickup)));
	if(!pP)
		return;

	pP->m_X = (int)m_Pos.x;
	pP->m_Y = (int)m_Pos.y;
	pP->m_Type = PICKUP_ARMOR;
}
