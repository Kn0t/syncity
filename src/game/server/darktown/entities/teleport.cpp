#include <game/server/gamecontext.h>
#include <game/server/gamecontroller.h>
#include <engine/shared/config.h>
#include "teleport.h"
#include "game/server/player.h"
#include "game/server/entities/character.h"

CTeleport::CTeleport(CGameWorld* pGameWorld, vec2 Pos, int Type)
    : CEntity(pGameWorld, CGameWorld::ENTTYPE_FLAG, Pos)
{
	m_Pos = Pos;

	for (int i = 0; i < 4; i++)
		m_IDs[i] = 0;

	m_Type = Type;
	GameWorld()->InsertEntity(this);
}

void CTeleport::Reset()
{
	GameServer()->m_World.DestroyEntity(this);
}

void CTeleport::To()
{	
	for(int i = 0; i < MAX_CLIENTS; i++)
	{
		CCharacter *pChar = GameServer()->GetPlayerChar(i);

		if(pChar)
		{
			for(int j = 0; j < 4; j++)
			{
				if(GameServer()->m_TeleNR[i] == m_IDs[j] && m_IDs[j] > 0)
				{
					if(GameServer()->m_TeleNum > 0)
					{
						if(!(GameServer()->Server()->Tick()%((rand()%20)+1)))
						{
							pChar->SetPosition(m_Pos);
							break;
						}
					}
					else
					{
						pChar->SetPosition(m_Pos);
						break;
					}
				}
			}
		}
	}
}

void CTeleport::From()
{	
	CCharacter *apEnts[MAX_CLIENTS];
	int Num = GameServer()->m_World.FindEntities(m_Pos, 0.0f, (CEntity**)apEnts, MAX_CLIENTS, CGameWorld::ENTTYPE_CHARACTER);
	for(int i = 0; i < Num; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			if(m_IDs[j] > 0)
			{
				GameServer()->m_TeleNR[apEnts[i]->GetPlayer()->GetCID()] = m_IDs[j];
				break;
			}
		}
	}	
}

void CTeleport::Tick()
{
	const int MAX_TILES = 5;

	for(int i = 0; i < 4; i++)
	{
		int STEPS = 32;
		int X, Y;

		switch(i)
		{
		case 0:
			X = -STEPS;
			Y = 0;
			break;
		case 1:
			X = 0;
			Y = -STEPS;
			break;
		case 2:
			X = STEPS;
			Y = 0;
			break;
		case 3:
			X = 0;
			Y = STEPS;
			break;
		}

		if(!m_IDs[i])
		{
			for(int j = 1; j <= MAX_TILES+1; j++)
			{
				int Number = -1;

				if(j < MAX_TILES)
				{
					vec2 TestPos = vec2(m_Pos.x+X*j, m_Pos.y +Y*j);
					Number = GameServer()->Collision()->GetID(TestPos);
				}


				if(Number != -1 && j < MAX_TILES+1)
				{
					if(i == 2 || i == 3)
					{
						m_IDs[i] += Number*pow(10.0f, MAX_TILES-j);
					}
					else
					{
						m_IDs[i] += Number*pow(10.0f, j-1);
					}
				}
				else
				{
					if((i == 2 || i == 3) && j < MAX_TILES+1)
						m_IDs[i] = m_IDs[i]/pow(10.0f, MAX_TILES+1-j);

					if(!m_IDs[i])
						m_IDs[i] = -1;
					
					break;
				}
			}
		}
	}

	if(m_Type)
		From();
	else
		To();
}

void CTeleport::Snap(int SnappingClient)
{
	if(NetworkClipped(SnappingClient, m_Pos))
		return;
}

