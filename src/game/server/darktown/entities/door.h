#ifndef GAME_SERVER_DARKTOWN_ENTITIES_DOOR
#define GAME_SERVER_DARKTOWN_ENTITIES_DOOR

#include <game/server/entity.h>

class CDoor : public CEntity {
private:
	void DoorID();
	void DoorNR();
	void Stop();
	void ItemID();
	int GetNumber();

	int m_Type;

	int m_aLength[4];
	int m_ItemID[4];
	int m_IDs[5];

public:
	CDoor(CGameWorld *pGameWorld, vec2 Pos, int Type);

    virtual void Reset();
	virtual void Tick();
	virtual void Snap(int SnappingClient);
};

#endif