/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */

#include <game/server/gamecontext.h>
#include <game/server/gamecontroller.h>
#include <game/server/darktown/shopitem.h>
#include <math.h>
#include <game/server/player.h>
#include <list>
#include "game/server/entities/character.h"

// shopitems
#include <game/server/darktown/shopitems/allweapons.h>
#include <game/server/darktown/shopitems/infammo.h>

#include "shopgui.h"

CShopGui::CShopGui(CGameWorld *pGameWorld, vec2 Pos, int Owner)
: CEntity(pGameWorld, CGameWorld::ENTTYPE_PICKUP, Pos)
{
	m_Owner = Owner;
	m_Visible = false;
	m_Pos = Pos;

	m_ItrFlag = true;
	m_Group = 0;
	m_Page = 0;

	// sorted by itemgroups, starting with ITEM_HAMMMER
	std::list<CEntity*> group;

	// general
	group.push_back(new CAllWeapons(GameWorld(), m_Owner, m_Pos));
	group.push_back(new CInfAmmo(GameWorld(), m_Owner, m_Pos));

	m_aShop.push_back(group);
	group.clear();
	
	// m_Menu = 1;
	// std::list<std::list<CEntity*>>::iterator it;
	// for(it = m_aShop.begin(); it != m_aShop.end(); ++it) {
	// 	std::list<CEntity*>::iterator entIt;
	// 	std::list<CEntity*>& pEnts = *it;
	// 	for(entIt = pEnts.begin(); entIt != pEnts.end(); ++entIt) {
	// 		CEntity *ent = *entIt;
	// 		// ent->m_Visible = true;
	// 	}
	// }

	GameWorld()->InsertEntity(this);
}

void CShopGui::Reset()
{
	GameServer()->m_World.DestroyEntity(this);
}

void CShopGui::Tick()
{
	CCharacter *pOwner = GameServer()->GetPlayerChar(m_Owner);

	if(!pOwner)
	{
		Reset();
		return;
	}
}

void CShopGui::Menu()
{
	CCharacter *pOwner = GameServer()->GetPlayerChar(m_Owner);

	if(!pOwner)
		return;

	std::list<std::list<CEntity*>>::iterator it;
	std::list<CEntity*>::iterator itE;
	
	if (!GameServer()->Collision()->CheckPoint(pOwner->m_Pos, TILE_SHOP)) { // we want to hide them only once
		if (m_ItrFlag) {
			for(it = m_aShop.begin(); it != m_aShop.end(); ++it) {
				std::list<CEntity*>::iterator entIt;
				std::list<CEntity*>& pEnts = *it;
				for(entIt = pEnts.begin(); entIt != pEnts.end(); ++entIt) {
					CEntity *ent = *entIt;
					ent->m_Visible = false;
				}
			}
			m_ItrFlag = false;
		}
		m_MsgFlag = true;
		return;
	} else
		m_ItrFlag = false;
		
	m_Group = pOwner->m_ShopGroup;

	if (m_MsgFlag) {
		GameServer()->SendChatTarget(pOwner->GetPlayer()->GetCID(), "Welcome to the Shop!");
		GameServer()->SendChatTarget(pOwner->GetPlayer()->GetCID(), "Type /shop for more informations");
		m_MsgFlag = false;
	}
	
	if (!m_ItrFlag) { // we only wanna do this, while on shop
		for(it = m_aShop.begin(); it != m_aShop.end(); ++it) {
			std::list<CEntity*>::iterator entIt;
			std::list<CEntity*>& pEnts = *it;
			for(entIt = pEnts.begin(); entIt != pEnts.end(); ++entIt) {
				CEntity *ent = *entIt;
				ent->m_Visible = false;
			}
		}

		int Size = next(m_aShop.begin(), m_Group)->size();
		int VisibleItems = Size - pOwner->m_ShopPage < 6 ? Size - pOwner->m_ShopPage : 6;
		int i = 1;


		if (pOwner->m_ShopPage < 0)
			pOwner->m_ShopPage = Size - Size % 6;
		else if (pOwner->m_ShopPage > Size - 1)
			pOwner->m_ShopPage = 0;
			
		std::list<CEntity*> Ents = *next(m_aShop.begin(), m_Group);

		for(itE = next(Ents.begin(), pOwner->m_ShopPage); (itE != next(Ents.begin(), pOwner->m_ShopPage + VisibleItems) && itE != Ents.end()); ++itE)
		{
			CEntity *Ent = *itE;

			if(!pOwner)
				return;

			Ent->m_Visible = true;
			if (!(VisibleItems % 2))
				Ent->m_Pos = pOwner->m_Pos + normalize(GetDir((2*pi/VisibleItems) * -i))*250;
			else
				Ent->m_Pos = pOwner->m_Pos + normalize(GetDir((2*pi/VisibleItems) * -i - 0.5*pi))*250;

			i++;
		}
		m_ItrFlag = true;
	}
}


void CShopGui::Snap(int SnappingClient)
{
    if(SnappingClient != m_Owner)
		return;

	Menu();
}
