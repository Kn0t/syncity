#ifndef GAME_SERVER_DARKTOWN_ENTITIES_TELEPORT
#define GAME_SERVER_DARKTOWN_ENTITIES_TELEPORT

#include <game/server/entity.h>

class CTeleport : public CEntity {
private:
	void From();
	void To();
	int m_IDs[4];
	int m_Type;

public:
	CTeleport(CGameWorld *pGameWorld, vec2 Pos, int Type);

    virtual void Reset();
	virtual void Tick();
	virtual void Snap(int SnappingClient);
};

#endif