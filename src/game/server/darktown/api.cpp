#include "engine/server/httplib.h"
#include "engine/server/json.hpp"
#include "game/server/gamecontext.h"
#include "game/server/player.h"

#include <game/server/darktown/authlevels.h>
#include <string> 

#include "api.h"

using json = nlohmann::json;

CApi::CApi(CGameContext *pGameServer) {
    m_pGameServer = pGameServer;
    m_Timeout = false;
}

bool CApi::Login(const char *username, const char *password, class CPlayer *pPlayer) {
    httplib::Client cli("localhost:3000");
    json j = {
        {"username", username},
        {"password", password}
    };
    if (auto res = cli.Post("/auth/login", j.dump().c_str(), "application/json")) {
        switch (res->status)
        {
        case 200:
        {
            json r = json::parse(res->body.c_str());

            if (r["token"].is_null()) {
                dbg_msg("api", "unexpected response. Login() status %d", res->status);
                dbg_msg("api", "%s", res->body.c_str());
                return false;
            }

            str_copy(pPlayer->m_Token, r["token"].get<std::string>().c_str(), sizeof(pPlayer->m_Token));

            GetAccountData(pPlayer);

            char aBuf[64];
            str_format(aBuf, sizeof(aBuf), "Welcome back %s :D", GameServer()->Server()->ClientName(pPlayer->GetCID()));

            GameServer()->SendChatTarget(pPlayer->GetCID(), aBuf);

            return true;
        }
        case 404:
            GameServer()->SendChatTarget(pPlayer->GetCID(), "This User doesnt exist");
            break;
        case 401:
            GameServer()->SendChatTarget(pPlayer->GetCID(), "Wrong Password");
            break;
        default:
            dbg_msg("api", "WARNING! Login unhandled response");
            GameServer()->SendChatTarget(pPlayer->GetCID(), "Whoops we failed, contact an Admin");

            break;
        }
    } else {
        // auto err = res.error();
        
        GameServer()->SendChatTarget(pPlayer->GetCID(), "Something went wrong :(. Try later again or contact an Admin");
    }

    return false;
}

bool CApi::Register(const char *username, const char *password, class CPlayer *pPlayer) {
    httplib::Client cli("localhost:3000");
    json j = {
        {"username", username},
        {"password", password}
    };

    if (auto res = cli.Post("/user", j.dump().c_str(), "application/json")) {
        switch (res->status)
        {
        case 201:
        {
            json r = json::parse(res->body.c_str());

            if (!r["token"].is_string()) {
                dbg_msg("api", "unexpected response. Login() status %d", res->status);
                dbg_msg("api", "%s", res->body.c_str());
                return false;
            }
            
            str_copy(pPlayer->m_Token, r["token"].get<std::string>().c_str(), sizeof(pPlayer->m_Token));

            GameServer()->SendChatTarget( pPlayer->GetCID(), "Welcome to DarkTown :D");

            return true;
        }
        case 409:
            GameServer()->SendChatTarget( pPlayer->GetCID(), "Username already taken");
            break;
        default:
            dbg_msg("api", "WARNING! Login unhandled response %d", res->status);
            GameServer()->SendChatTarget(pPlayer->GetCID(), "Whoops we failed, contact an Admin");

            break;
        }
        
    } else {
        // auto err = res.error();
        GameServer()->SendChatTarget(pPlayer->GetCID(), "Something went wrong :(. Try later again or contact an Admin");
    }

    return false;
}

void CApi::GetAccountData(CPlayer *pPlayer) {
    httplib::Client cli("localhost:3000");
    char aToken[256];

    str_format(aToken, sizeof(aToken), "Barear %s", pPlayer->m_Token);
    httplib::Headers Headers = {
        { "Authorization", aToken }
    };

    if (auto res = cli.Get("/user/token", Headers)) {
        switch (res->status)
        {
        case 200:
        {
            json r = json::parse(res->body.c_str());
            
            pPlayer->m_AccData.ID = r["_id"].get<int>();
            pPlayer->m_AccData.Level = r["level"].get<int>();
            pPlayer->m_AccData.Exp = (long long)r["exp"].get<int>();
            pPlayer->m_AccData.Money = (long long)r["money"].get<int>();
            pPlayer->m_AccData.AuthLvl = r["authLvl"].get<int>();
            pPlayer->m_AccData.RaceHighscore = r["raceHighscore"].is_null() ? -1 : r["raceHighscore"].get<int>();
            pPlayer->m_AccData.Vip = r["roles"]["vip"].get<int>();
            pPlayer->m_AccData.Donor = r["roles"]["donor"].get<int>();
            pPlayer->m_AccData.AllWeapons = r["items"]["allweapons"].get<int>();

            switch (pPlayer->m_AccData.AuthLvl)
            {
            case AUTH_ADMIN:
                GameServer()->Server()->SetRconlvl(pPlayer->GetCID(), 2);
                break;
            case AUTH_MOD:
                GameServer()->Server()->SetRconlvl(pPlayer->GetCID(), 1);
                break;
            default:
                break;
            }
            break;
        }
        default:
            break;
        }
    }
}

void CApi::UpdateAccount(CPlayer *pPlayer) {
    if (m_Timeout || !pPlayer->isLogged()) {
        return;
    }
    
    httplib::Client cli("localhost:3000");
    json j = {
        {"level", pPlayer->m_AccData.Level},
        {"exp", pPlayer->m_AccData.Exp},
        {"money", pPlayer->m_AccData.Money},
        {"authLvl", pPlayer->m_AccData.AuthLvl},
        {"nickname", GameServer()->Server()->ClientName(pPlayer->GetCID())},
        {"raceHighscore", pPlayer->m_AccData.RaceHighscore},
        {"roles", {
            {"vip", pPlayer->m_AccData.Vip},
            {"donor", pPlayer->m_AccData.Donor}
        }},
        {"items", {
            {"allweapons", pPlayer->m_AccData.AllWeapons}
        }}
    };

    char aToken[256];
    str_format(aToken, sizeof(aToken), "Barear %s", pPlayer->m_Token);

    httplib::Headers Headers = {
        { "Authorization", aToken },
        { "From", "game" },
    };

    if (auto res = cli.Put("/user", Headers, j.dump().c_str(), "application/json")) {

    } else {
        dbg_msg("DEBUG", "%d", res.error());
        m_Timeout = true;
    }
    
    // TODO handle errors
}

void CApi::Top10(int ClientID, const char *List) {
    httplib::Client cli("localhost:3000");
    char aBuf[64];
    str_format(aBuf, sizeof(aBuf), "/user/top/%s", List);

    if (auto res = cli.Get(aBuf)) {
        if (res->status == 200) {
            json j = json::parse(res->body.c_str());

            GameServer()->SendChatTarget(ClientID, "~~~ Top10 Races ~~~");

            for (auto& el : j.items()) {
                char aScore[32];
                tick_to_time(el.value()["raceHighscore"].get<int>(), aScore, sizeof(aScore));
                std::string Username = el.value()["nickname"].get<std::string>();
                str_format(aBuf, sizeof(aBuf), "#%d %s : %s", atoi(el.key().c_str()) + 1, Username.c_str(), aScore);
                GameServer()->SendChatTarget(ClientID, aBuf);
            }
        }
    } else {
        dbg_msg("DEBUG", "%d", res.error());
        m_Timeout = true;
    }
}