#include "engine/server/websocket.h"
#include "engine/server/json.hpp"
#include "game/server/gamecontext.h"
#include "game/server/gamecontroller.h"
#include "game/server/player.h"
#include "game/server/darktown/api.h"

#include "socket.h"

using json = nlohmann::json;

CSocket::CSocket(CGameContext *pGameServer) {
    m_pGameServer = pGameServer;
}

void CSocket::Run(void *pUser) {
    CSocket *pThis = (CSocket *)pUser;

    if (!pThis->m_WSclient.wsConnect(3000, "localhost", 8080, "/", "localhost:8080", (const char *)nullptr, "echo-protocol")) {
        dbg_msg("socket", "Couldn't connect to the WS Server");
        return;
    }

    if (pThis->m_Running.load(std::memory_order_relaxed)) {
        return;
    }

    pThis->m_Running = true;
    
    pThis->m_WSThread = std::thread([pThis]() {
      while (pThis->m_Running.load(std::memory_order_relaxed) && pThis->m_WSclient.isConnected()) {
        {
          std::lock_guard<std::mutex> lck(pThis->m_Mtx);
          pThis->m_WSclient.poll(pThis);
        }
        std::this_thread::yield();
      }
    });

    dbg_msg("socket", "WS connection established");
    std::string line;
    while (pThis->m_Running.load(std::memory_order_relaxed) && pThis->m_WSclient.isConnected() && std::getline(std::cin, line)) {
        std::lock_guard<std::mutex> lck(pThis->m_Mtx);
        pThis->m_WSclient.send(websocket::OPCODE_TEXT, (const uint8_t*)line.data(), line.size());
    }
    pThis->Stop();

    pThis->m_WSThread.join();
    dbg_msg("socket", "WS Client stopped");
};

void CSocket::onWSClose(WSConn& Conn, uint16_t StatusCode, const char* Reason) {
    dbg_msg("socket", "Ws closed with code: %d, reason: %s", StatusCode, Reason);
}

void CSocket::onWSMsg(WSConn& Conn, uint8_t OPcode, const uint8_t* Payload, uint32_t PLLen) {
    if (OPcode == websocket::OPCODE_PING) {
        Conn.send(websocket::OPCODE_PONG, Payload, PLLen);
        return;
    }

    if (OPcode != websocket::OPCODE_TEXT) {
        dbg_msg("socket", "Got none msg, OPcode: %d", (int)OPcode);
        return;
    }

    char aMsg[1028];
    ParseMessage((const char*)Payload, aMsg);
    HandleMessage(aMsg);
}

void CSocket::onWSSegment(WSConn& Conn, uint8_t OPcode, const uint8_t* Payload, uint32_t PLLen, uint32_t PLStartIdx, bool Fin) {
    dbg_msg("socket", "onWSSegment");
}

void CSocket::ParseMessage(const char *Payload, char *Out) {
    std::string PayloadStr = Payload;
    str_copy(Out, PayloadStr.substr(0, PayloadStr.find_first_of('#')).c_str(), 512);
}

void CSocket::HandleMessage(const char *Msg) {
    json j = json::parse(Msg);
    char aType[16];

    if (!j["type"].is_string()) {
        dbg_msg("socket", "Invalid Message: %s", Msg);
    }

    str_copy(aType, j["type"].get<std::string>().c_str(), sizeof(aType));

    if (str_comp_nocase(aType, "chat-all") == 0) {
        char aText[256];
        str_copy(aText, j["data"]["msg"].get<std::string>().c_str(), sizeof(aType));

        GameServer()->SendChat(-1, 0, -1, aText);
    } else if (str_comp_nocase(aType, "update") == 0) {
        if (str_comp_nocase(j["data"]["from"].get<std::string>().c_str(), "game") == 0)
            return;

        int AccID = j["data"]["_id"].get<int>();
        int ClientID = GameServer()->GetClientIdByAccID(AccID);
        if (ClientID == -1)
            return;

        dbg_msg("DEBUG", "%d ingame %d", AccID, ClientID);

        GameServer()->m_pController->Api()->GetAccountData(GameServer()->m_apPlayers[ClientID]);
    } else {
        dbg_msg("socket", "Unhandled Msg of Type '%s' Msg: %s", aType, Msg);
    }
}
