#ifndef GAME_SERVER_DARKTOWN_SHOPITEM
#define GAME_SERVER_DARKTOWN_SHOPITEM

#include <game/server/entity.h>

class CShopItem : public CEntity {
private:

public:
    CShopItem(CGameWorld *pGameWorld, int Owner, vec2 Pos, const char *Name, long long Price);

    virtual void GiveItem() {};

    virtual void Tick();

    bool Buy();

    int m_OwnerID;
    int m_MinLvl;
    int *m_pItem;
    long long m_Price;
    char m_aName[256];

    CCharacter *m_pOwner;
};

#endif