#include <game/server/gamecontext.h>
#include <game/server/gamecontroller.h>
#include <engine/shared/config.h>
#include <game/server/darktown/api.h>
#include "shopitem.h"
#include "game/server/entities/character.h"
#include "game/server/player.h"

CShopItem::CShopItem(CGameWorld *pGameWorld, int Owner, vec2 Pos, const char *Name, long long Price)
    : CEntity(pGameWorld, CGameWorld::ENTTYPE_FLAG, Pos)
{
	m_Pos = Pos;
    m_pOwner = GameServer()->GetPlayerChar(Owner);
    m_OwnerID = Owner;
    str_copy(m_aName, Name, sizeof(m_aName));
    m_Price = Price;
}

void CShopItem::Tick() {
    if(!m_pOwner || !m_pOwner->IsAlive())
	{
		Reset();
		return;
	}

	if(!m_Visible)
		return;

	int Click = m_pOwner->GetMouseEvent(m_Pos);

    if (Click == 0) {
        char aBuf[128], aNumBuf[16];
         str_format_int(m_Price, aNumBuf);

        if (*m_pItem) {
            str_format(aBuf, sizeof(aBuf), "%s\n%s$\nAlready bought", m_aName, aNumBuf);
        }
        else {
            str_format(aBuf, sizeof(aBuf), "%s\n%s$", m_aName, aNumBuf);
        }

        GameServer()->SendBroadcast(aBuf, m_OwnerID);
        return;
    } else if(Click != 2) {
		m_LastClick = 0;
		return;
	}

    if (m_LastClick + Server()->TickSpeed() > Server()->Tick()) {
        return;
    }

    m_LastClick = Server()->Tick();
    Buy();
}

bool CShopItem::Buy() {
    CPlayer *pPlayer = m_pOwner->GetPlayer();
    char aBuf[128];

    if (*m_pItem) {
        str_format(aBuf, sizeof(aBuf), "You already bought %s", m_aName);
        GameServer()->SendChatTarget(m_OwnerID, aBuf);
        return false;
    }

    if (pPlayer->m_AccData.Money < m_Price) {
        GameServer()->SendChatTarget(m_OwnerID, "You dont have enough Money");
        return false;
    }

    if (pPlayer->m_AccData.Level < m_MinLvl) {
        str_format(aBuf, sizeof(aBuf), "You need to be at least level %d", m_MinLvl);
        GameServer()->SendChatTarget(m_OwnerID, aBuf);
        return false;
    }

    pPlayer->m_AccData.Money -= m_Price;
    str_format(aBuf, sizeof(aBuf), "You bought %s", m_aName);
    GameServer()->SendChatTarget(m_OwnerID, aBuf);
    *m_pItem = 1;
    GiveItem();
    GameServer()->m_pController->Api()->UpdateAccount(m_pOwner->GetPlayer());
    return true;
}