/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#include <game/server/gamecontroller.h>
#include <game/server/gamecontext.h>
#include <game/server/entities/character.h>
#include <game/server/player.h>
#include "allweapons.h"

CAllWeapons::CAllWeapons(CGameWorld *pGameWorld, int Owner, vec2 Pos)
	: CShopItem(pGameWorld, Owner, Pos, "All Weapons", 0)
{
	m_pOwner = GameServer()->GetPlayerChar(Owner);
	m_OwnerID = Owner;
	m_Pos = Pos;
	m_pItem = &GameServer()->m_apPlayers[m_OwnerID]->m_AccData.AllWeapons;

	for(int i = 0; i < 10; i++)
		m_IDs[i] = Server()->SnapNewID();

	GameWorld()->InsertEntity(this);
}

void CAllWeapons::Reset()
{
	GameServer()->m_World.DestroyEntity(this);

	for(int i = 0; i < 10; i++)
		Server()->SnapFreeID(m_IDs[i]);
}
	
void CAllWeapons::GiveItem() {
	if (m_pOwner) {
		CPlayer *pPlayer = m_pOwner->GetPlayer();
    	m_pOwner->GetPlayer()->m_AccData.AllWeapons = true;
		m_pOwner->GiveWeapon(WEAPON_SHOTGUN, pPlayer->m_AccData.InfAmmo ? -1 : 10);
		m_pOwner->GiveWeapon(WEAPON_GRENADE, pPlayer->m_AccData.InfAmmo ? -1 : 10);
		m_pOwner->GiveWeapon(WEAPON_LASER, pPlayer->m_AccData.InfAmmo ? -1 : 10);
		m_pOwner->GiveNinja();
	}
}

void CAllWeapons::Snap(int SnappingClient)
{
	if(SnappingClient != m_OwnerID || NetworkClipped(SnappingClient, m_Pos) || !m_Visible)
		return;

	CNetObj_Pickup *pP[6];

	for(int i = 0; i < 5; i++)
	{
		pP[i] = static_cast<CNetObj_Pickup *>(Server()->SnapNewItem(NETOBJTYPE_PICKUP, m_IDs[i], sizeof(CNetObj_Pickup)));

		if(!pP[i])
			return;

		if(i != 4)
		{
			pP[i]->m_X = (int)m_Pos.x;
			pP[i]->m_Y = (int)m_Pos.y + 10 + 20 - i * 20;
			pP[i]->m_Type = PICKUP_GRENADE + i;
		}
		else
		{
			pP[i]->m_X = (int)m_Pos.x;
			pP[i]->m_Y = (int)m_Pos.y;
			pP[i]->m_Type = PICKUP_HAMMER;
		}

	}	
}
