/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#include <game/server/gamecontroller.h>
#include <game/server/gamecontext.h>
#include <game/server/entities/character.h>
#include <game/server/player.h>
#include "infammo.h"

CInfAmmo::CInfAmmo(CGameWorld *pGameWorld, int Owner, vec2 Pos)
	: CShopItem(pGameWorld, Owner, Pos, "Infinit Ammo", 0)
{
	m_pOwner = GameServer()->GetPlayerChar(Owner);
	m_OwnerID = Owner;
	m_Pos = Pos;
	m_pItem = &GameServer()->m_apPlayers[m_OwnerID]->m_AccData.InfAmmo;

	for(int i = 0; i < 10; i++)
		m_IDs[i] = Server()->SnapNewID();

	GameWorld()->InsertEntity(this);
}

void CInfAmmo::Reset()
{
	GameServer()->m_World.DestroyEntity(this);

	for(int i = 0; i < 10; i++)
		Server()->SnapFreeID(m_IDs[i]);
}
	
void CInfAmmo::GiveItem() {
	if (m_pOwner) {
    	m_pOwner->GetPlayer()->m_AccData.AllWeapons = true;
	}
}

void CInfAmmo::Snap(int SnappingClient)
{
	if(SnappingClient != m_OwnerID || NetworkClipped(SnappingClient, m_Pos) || !m_Visible)
		return;

	CNetObj_Projectile *pProj[4];

	for(int i = 0; i < 4; i++)
	{
		pProj[i] = static_cast<CNetObj_Projectile *>(Server()->SnapNewItem(NETOBJTYPE_PROJECTILE, m_IDs[i], sizeof(CNetObj_Projectile)));

		if(!pProj[i])
			return;

		pProj[i]->m_Type = WEAPON_GUN + i;
		pProj[i]->m_StartTick = m_StartTick;
		pProj[i]->m_VelX = 0;
		pProj[i]->m_VelY = 0;
		pProj[i]->m_X = (int)m_Pos.x;
		pProj[i]->m_Y = (int)m_Pos.y;

		switch (i)
		{
		case 0:
			pProj[i]->m_X += 16;
			break;
		case 1:
			pProj[i]->m_X -= 16;
			break;
		case 2:
			pProj[i]->m_Y += 16;
			break;
		case 3:
			pProj[i]->m_Y -= 16;
			break;
		default:
			break;
		}
	}	
}
