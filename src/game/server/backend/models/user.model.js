const mongoose = require('mongoose');
const { ADMIN, NONE } = require('../config/authlevel.config');
require('../config/authlevel.config')
const AutoIncrement = require('mongoose-sequence')(mongoose);

const UserSchema = mongoose.Schema({
    _id: Number,
    username: {
        type: String,
        require: true,
        unique: true
    },
    password: {
        type: String,
        require: true
    },
    token: String,
    authLvl: {
        type: Number,
        min: ADMIN,
        max: NONE,
        default: NONE,
        require: true
    },
    nickname: {
        type: String,
        default: 'namless tee'
    },
    level: {    
        type: Number,
        default: 1
    },
    exp: {
        type: Number,
        default: 0
    },
    money: {
        type: Number,
        default: 0
    },
    raceHighscore: {
        type: Number,
        default: null
    },
    roles: {
        vip: {
            type: Number,
            default: 0
        },
        donor: {
            type: Number,
            default: 0
        }
    },
    items: {
        allweapons: {
            type: Number,
            default: 0
        }
    }
    
}, {_id: false});

UserSchema.plugin(AutoIncrement, {inc_field: '_id'});

module.exports = mongoose.model('User', UserSchema);