const express = require('express');
const router = express.Router();
const userController = require('../controller/user.controller');
const authController = require('../controller/auth.controller');
const socket = require('../controller/socket.controller');
const { updateData, wsrObject } = require('../factories/wsr.factory');

// create
router.post('/', async (req, res) => {

    if (!(req.body.username && req.body.password)) {
        res.sendStatus(400);
        return;
    }
    
    let user = await userController.get({username: req.body.username});

    if (user === 500) {
        res.sendStatus(500);
        return;
    } else if (user.length > 0) {
        res.sendStatus(409);
        return;
    }

    const hash = authController.passHash(req.body.password);

    await userController.create({
        username: req.body.username,
        password: hash
    });
    user = (await userController.get({username: req.body.username}))[0];

    user.token = authController.getToken(user._id, 0);

    try {
        await userController.update(user._id, user);
    } catch (err) {
        res.sendStatus(500);
        console.error(err);
        return;
    }

    res.status(201).json(user);
});

// get
router.get('/', async (req, res) => {
    res.json(await userController.get(req.query));
});

// get by token
router.get('/token', async (req, res) => {
    const token = authController.getTokenByHeader(req.headers);
    res.json(await userController.getById(token.id));
});

// update
router.put('/', authController.needToken, async (req, res) => {
    const token = authController.getTokenByHeader(req.headers);
    res.sendStatus(await userController.update(token.id, req.body));
    
    const wsr = wsrObject('update', updateData(token.id, req.headers['from']));
    socket.sendMsg(JSON.stringify(wsr));
});

// delete
router.delete('/', authController.needToken, async (req, res) => {
    res.sendStatus(await userController.delete(req.query));
});

// get top10 race
router.get('/top/race', async (req, res) => {
    res.json(await userController.get(
        {},
        ['nickname', 'raceHighscore'],
        {
            limit: 10,
            sort: {
                raceHighscore: 1
            }
        }));
});

module.exports = router;