const express = require('express');
const router = express.Router();
const userController = require('../controller/user.controller');
const authController = require('../controller/auth.controller');
const socket = require('../controller/socket.controller');
const { wsrObject, loginData } = require('../factories/wsr.factory');
// const socket = require('../socket');

router.post('/token', async (req, res) => {
    if (!req.body.id) {
        res.sendStatus(400);
        return;
    }
    const user = (await userController.get({_id: req.body.id}))[0];
    res.json({token: authController.getToken(user._id, user.authLvl)});
});

router.post('/login', async (req, res) => {
    if (!(req.body.password && req.body.username)) {
        res.sendStatus(400);
        return;
    }

    const user = (await userController.get({username: req.body.username}))[0];

    if (!user) {
        res.sendStatus(404);
        return;
    }

    if (authController.compairHash(req.body.password, user.password)) {
        res.json({token: user.token});

        // WebSocket Response example
        const wsr = wsrObject('login', loginData(user.username));
        socket.sendMsg(JSON.stringify(wsr));
        return;
    } else {
        res.sendStatus(401);
        return;
    }
});

module.exports = router;