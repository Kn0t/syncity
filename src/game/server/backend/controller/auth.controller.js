const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const authLvls = require('../config/authlevel.config');

exports.getToken = (id, authLvl) => {
    try {
        return jwt.sign({id, authLvl}, process.env.TOKEN_KEY);
    } catch (err) {
        console.error(err);
        return null;
    }
}

exports.decodeToken = (token) => {
    try {
        return jwt.decode(token);
    } catch (err) {
        console.error(err);
        return null;
    }
}

exports.passHash = (pass) => {
    try {
        return bcrypt.hashSync(pass, 10);
    } catch (err) {
        console.error(err);
        return null;
    }
}

exports.compairHash = (plain, hash) => {
    try {
        return bcrypt.compareSync(plain, hash);
    } catch(err) {
        console.error(err);
        return false;
    }
}

exports.needToken = (req, res, next) => {
    const token = this.decodeToken(req.headers['authorization'].split(' ')[1]);

    if (!token) {
        res.sendStatus(401);
        return;
    }

    next();
}

exports.isAdmin = (req, res, next) => {
    if (!isAuthed(this.getTokenByHeader(req.headers).authLvl, authLvls.ADMIN)) {
        res.sendStatus(401);
        return;
    }

    next();
}

exports.isMod = (req, res, next) => {
    if (!isAuthed(this.getTokenByHeader(req.headers).authLvl, authLvls.MOD)) {
        res.sendStatus(401);
        return;
    }

    next();
}

exports.isMapper = (req, res, next) => {
    if (!isAuthed(this.getTokenByHeader(req.headers).authLvl, authLvls.MAPPER)) {
        res.sendStatus(401);
        return;
    }

    next();
}

exports.isPolice = (req, res, next) => {
    if (!isAuthed(this.getTokenByHeader(req.headers).authLvl, authLvls.POLICE)) {
        res.sendStatus(401);
        return;
    }

    next();
}

exports.getTokenByHeader = (headers) => {
    return this.decodeToken(headers['authorization'].split(' ')[1]);
}

function isAuthed(usrLevel, authLvl) {
    return usrLevel != null ? usrLevel <= authLvl : false;
}