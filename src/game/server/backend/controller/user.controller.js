const UserModel = require('../models/user.model');

exports.create = async (userData) => {
    try {
        const user = new UserModel(userData);
        await user.save();

        return user;
    } catch(err) {
        console.error(err);
        return 500;
    }

}

exports.get = async (query, cols = [], options = {}) => {
    try {
        return await UserModel.find(query, cols, options);
    } catch (err) {
        console.error(err);
        return 500;
    }
}

exports.getById = async (id) => {
    try {
        return await UserModel.findById(id);
    } catch (err) {
        console.error(err);
        return 500;
    }
}

exports.update = async (id, userData) => {
    try {
        await UserModel.findByIdAndUpdate(id, userData);

        return 201;
    } catch (err) {
        console.error(err);
        return 500;
    }
}

exports.delete = async (query) => {
    try {
        await UserModel.deleteMany(query);
        return 200;
    } catch (err) {
        console.error(err);
        return 500;
    }
}