const WebSocketClient = require('websocket').client;
const client = new WebSocketClient();

client.on('connectFailed', function(error) {
    console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(connection) {
    console.log('WebSocket Client Connected');
    connection.on('error', function(error) {
        console.log("WS Connection Error: " + error.toString());
    });
    connection.on('close', function() {
        console.log('echo-protocol Connection Closed');
    });
    
    exports.sendMsg = (msg) => {
        if (connection.connected) {
            connection.sendUTF(msg);
        }
    }
});

client.connect('ws://localhost:8080/', 'echo-protocol');