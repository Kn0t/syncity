
// we define how an WebSocket reponse look here

exports.wsrObject = (type, data) => {
    let wsr = {};

    wsr.type = type;
    wsr.data = data;

    return wsr;
}

exports.loginData = (username) => {
    let data = {};

    data.username = username;

    return data;
}

exports.updateData = (id, from) => {
    let data = {};

    data._id = id;
    data.from = from;

    return data;
}