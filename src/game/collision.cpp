/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#include <base/system.h>
#include <base/math.h>
#include <base/vmath.h>

#include <math.h>
#include <engine/map.h>
#include <engine/kernel.h>
#include <game/server/gamecontext.h>
#include <game/server/gamecontroller.h>
#include <engine/shared/config.h>

#include <game/mapitems.h>
#include <game/layers.h>
#include <game/collision.h>


CCollision::CCollision()
{
	m_pTiles = 0;
	m_Width = 0;
	m_Height = 0;
	m_pLayers = 0;

	// darktown
	for (int i = 0; i <= 16; i++) {
		m_pAllTiles[i] = 0;
	}
}

void CCollision::Init(class CLayers *pLayers)
{
	m_pLayers = pLayers;
	m_Width = m_pLayers->GameLayer()->m_Width;
	m_Height = m_pLayers->GameLayer()->m_Height;
	m_pTiles = static_cast<CTile *>(m_pLayers->Map()->GetData(m_pLayers->GameLayer()->m_Data));

	m_pAllTiles[0] = new CTile[m_Width*m_Height];
	m_pIDTile = new CTile[m_Width*m_Height];

	mem_copy(m_pAllTiles[0], m_pTiles, sizeof(CTile)*m_Width*m_Height);

	// init
	m_pIDTile = static_cast<CTile *>(m_pLayers->Map()->GetData(m_pLayers->IDLayer()->m_Data));

	for (int i = 0; i < NUM_DOORS; i++) {
		m_pDoorTiles[i] = new CDoorTile[m_Width * m_Height];

		for (int j = 0; j < 1024; j++) {
			m_DoorOpen[i][j] = false;
		}
	}
	

	for (int i = 0; i < m_pLayers->MAX_GAME_LAYERS; i++) {
		if (!m_pLayers->SubGameLayers(i))
			break;

		int Width = m_pLayers->SubGameLayers(i)->m_Width;
		int Height = m_pLayers->SubGameLayers(i)->m_Height;

		CTile *CastTile = static_cast<CTile *>(m_pLayers->Map()->GetData(m_pLayers->SubGameLayers(i)->m_Data));
		
		m_pAllTiles[i+1] = new CTile[Width*Height];
		mem_copy(m_pAllTiles[i+1], CastTile, sizeof(CTile)*Width*Height);

		m_TotalTiles++;
	}

	for (int i = 0; i < m_Width*m_Height; i++) {
		int Index = m_pTiles[i].m_Index;

		for (int j = 0; j < NUM_DOORS; j++) {
			m_pDoorTiles[j][i].m_Index = 0;
			m_pDoorTiles[j][i].m_DoorID = 0;
		}

		if(Index > 128)
			continue;

		switch(Index)
		{
			case TILE_DEATH:
				m_pTiles[i].m_Index = COLFLAG_DEATH;
				break;
			case TILE_SOLID:
				m_pTiles[i].m_Index = COLFLAG_SOLID;
				break;
			case TILE_NOHOOK:
				m_pTiles[i].m_Index = COLFLAG_SOLID|COLFLAG_NOHOOK;
				break;
			default:
				m_pTiles[i].m_Index = 0;
		}
	}
}

int CCollision::GetTile(int x, int y) const
{
	int Nx = clamp(x/32, 0, m_Width-1);
	int Ny = clamp(y/32, 0, m_Height-1);

	if (GetDoorAt(x, y)) {
		// todo: make it configurable
		return TILE_SOLID;
	}

	return m_pTiles[Ny*m_Width+Nx].m_Index > 128 ? 0 : m_pTiles[Ny*m_Width+Nx].m_Index;
}

bool CCollision::IsTile(int x, int y, int Type) const
{
	int Nx = clamp(x/32, 0, m_Width-1);
	int Ny = clamp(y/32, 0, m_Height-1);

	if (Type == TILE_SOLID && GetDoorAt(x, y)) {
		// todo: make it configurable
		return TILE_SOLID;
	}

	for (int i = 0; i <= m_pLayers->MAX_GAME_LAYERS; i++) {
		if (!m_pAllTiles[i])
			break;

		if (m_pAllTiles[i][Ny*m_Width+Nx].m_Index == Type)
			return true;
	}

	return false;
}

int CCollision::GetID(int x, int y) const
{
	int Nx = clamp(x/32, 0, m_Width-1);
	int Ny = clamp(y/32, 0, m_Height-1);

	return m_pIDTile[Ny*m_Width+Nx].m_Index - 1;
}

bool CCollision::SetDoorState(int DoorNumber, bool State, int Type) {
	m_DoorOpen[Type][DoorNumber] = State;
	return m_DoorOpen[Type][DoorNumber];
}

bool CCollision::GetDoorAt(int x, int y) const
{
	int Nx = clamp(x / 32, 0, m_Width - 1);
	int Ny = clamp(y / 32, 0, m_Height - 1);

	for (int i = 0; i < NUM_DOORS; i++) {
		if (m_pDoorTiles[i][Ny * m_Width + Nx].m_Index == 7)
		{
			if (!m_DoorOpen[i][m_pDoorTiles[i][Ny * m_Width + Nx].m_DoorID]) {
				return true;
			}
		}
	}

	return false;
}

int CCollision::SetDoorAt(vec2 From, vec2 To, int Number, int Type)
{
	float Distance = distance(From, To);
	int End(Distance + 1);

	for (int i = 0; i < End; i++)
	{
		float a = i / Distance;
		vec2 Pos = mix(From, To, a);

		int Nx = clamp((int)Pos.x / 32, 0, m_Width - 1);
		int Ny = clamp((int)Pos.y / 32, 0, m_Height - 1);

		if (Number)
		{
			m_pDoorTiles[Type][Ny * m_Width + Nx].m_DoorID = Number;
			m_pDoorTiles[Type][Ny * m_Width + Nx].m_Index = 7;
		}
		else
		{
			m_pDoorTiles[Type][Ny * m_Width + Nx].m_DoorID = 0;
			m_pDoorTiles[Type][Ny * m_Width + Nx].m_Index = 0;
		}
	}

	return 1;
}


// TODO: rewrite this smarter!
int CCollision::IntersectLine(vec2 Pos0, vec2 Pos1, vec2 *pOutCollision, vec2 *pOutBeforeCollision) const
{
	const int End = distance(Pos0, Pos1)+1;
	const float InverseEnd = 1.0f/End;
	vec2 Last = Pos0;

	for(int i = 0; i <= End; i++)
	{
		vec2 Pos = mix(Pos0, Pos1, i*InverseEnd);
		if(CheckPoint(Pos.x, Pos.y))
		{
			if(pOutCollision)
				*pOutCollision = Pos;
			if(pOutBeforeCollision)
				*pOutBeforeCollision = Last;
			return GetCollisionAt(Pos.x, Pos.y);
		}
		Last = Pos;
	}
	if(pOutCollision)
		*pOutCollision = Pos1;
	if(pOutBeforeCollision)
		*pOutBeforeCollision = Pos1;
	return 0;
}

// TODO: OPT: rewrite this smarter!
void CCollision::MovePoint(vec2 *pInoutPos, vec2 *pInoutVel, float Elasticity, int *pBounces) const
{
	if(pBounces)
		*pBounces = 0;

	vec2 Pos = *pInoutPos;
	vec2 Vel = *pInoutVel;
	if(CheckPoint(Pos + Vel))
	{
		int Affected = 0;
		if(CheckPoint(Pos.x + Vel.x, Pos.y))
		{
			pInoutVel->x *= -Elasticity;
			if(pBounces)
				(*pBounces)++;
			Affected++;
		}

		if(CheckPoint(Pos.x, Pos.y + Vel.y))
		{
			pInoutVel->y *= -Elasticity;
			if(pBounces)
				(*pBounces)++;
			Affected++;
		}

		if(Affected == 0)
		{
			pInoutVel->x *= -Elasticity;
			pInoutVel->y *= -Elasticity;
		}
	}
	else
	{
		*pInoutPos = Pos + Vel;
	}
}

bool CCollision::TestBox(vec2 Pos, vec2 Size, int Flag) const
{
	Size *= 0.5f;
	if(CheckPoint(Pos.x-Size.x, Pos.y-Size.y, Flag))
		return true;
	if(CheckPoint(Pos.x+Size.x, Pos.y-Size.y, Flag))
		return true;
	if(CheckPoint(Pos.x-Size.x, Pos.y+Size.y, Flag))
		return true;
	if(CheckPoint(Pos.x+Size.x, Pos.y+Size.y, Flag))
		return true;
	return false;
}

void CCollision::MoveBox(vec2 *pInoutPos, vec2 *pInoutVel, vec2 Size, float Elasticity, bool *pDeath) const
{
	// do the move
	vec2 Pos = *pInoutPos;
	vec2 Vel = *pInoutVel;

	const float Distance = length(Vel);
	const int Max = (int)Distance;

	if(pDeath)
		*pDeath = false;

	if(Distance > 0.00001f)
	{
		const float Fraction = 1.0f/(Max+1);
		for(int i = 0; i <= Max; i++)
		{
			vec2 NewPos = Pos + Vel*Fraction; // TODO: this row is not nice

			//You hit a deathtile, congrats to that :)
			//Deathtiles are a bit smaller
			if(pDeath && TestBox(vec2(NewPos.x, NewPos.y), Size*(2.0f/3.0f), COLFLAG_DEATH))
			{
				*pDeath = true;
			}

			if(TestBox(vec2(NewPos.x, NewPos.y), Size))
			{
				int Hits = 0;

				if(TestBox(vec2(Pos.x, NewPos.y), Size))
				{
					NewPos.y = Pos.y;
					Vel.y *= -Elasticity;
					Hits++;
				}

				if(TestBox(vec2(NewPos.x, Pos.y), Size))
				{
					NewPos.x = Pos.x;
					Vel.x *= -Elasticity;
					Hits++;
				}

				// neither of the tests got a collision.
				// this is a real _corner case_!
				if(Hits == 0)
				{
					NewPos.y = Pos.y;
					Vel.y *= -Elasticity;
					NewPos.x = Pos.x;
					Vel.x *= -Elasticity;
				}
			}

			Pos = NewPos;
		}
	}

	*pInoutPos = Pos;
	*pInoutVel = Vel;
}
